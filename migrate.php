<?php
// include database
include_once 'database.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

$sql = "
		CREATE DATABASE IF NOT EXISTS `flip_test` DEFAULT CHARACTER SET utf8;

		USE `flip_test`;

		/*Table structure for table `transaction` */

		DROP TABLE IF EXISTS `transaction`;

		CREATE TABLE `transaction` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `trans_id` varchar(20) DEFAULT NULL,
		  `amount` int(11) DEFAULT NULL,
		  `status` varchar(10) DEFAULT NULL,
		  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `bank_code` varchar(50) DEFAULT NULL,
		  `account_number` varchar(20) DEFAULT NULL,
		  `beneficiary_name` varchar(100) DEFAULT NULL,
		  `remark` text,
		  `receipt` text,
		  `time_served` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
		  `fee` int(11) DEFAULT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
";
$query = $db->prepare($sql);

try {
	    $db->beginTransaction();

		if($query->execute()) {
			$db->commit();

			echo "Database flip_test and transaction table has been created";
		} else {
			$db->rollback();

			echo "Failed to create database and table";
		}
} catch (PDOException $e) {
	$db->rollback();

	echo "Failed to create database and table";
}
?>
