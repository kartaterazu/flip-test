<?php
// include database, helper and model
include_once 'database.php';
include_once 'Helper.php';
include_once 'model/Transaction.php';

$transaction_id = getopt('p:')['p'] ? getopt('p:')['p'] : ($_GET['id'] ?? '');

// get database connection
$database = new Database();
$db = $database->getConnection();

$data = [
    "bank_code" 		=> "mandiri",
    "account_number"	=> "1234567890",
    "amount"			=> "200000",
    "remark"			=> "testing"
];

$uri = !empty($transaction_id) ? "/{$transaction_id}" : '';

$helper = new Helper();
$helper->url 	= "https://nextar.flip.id/disburse{$uri}";
$helper->data 	= empty($uri) ? $data : [];

$data = $helper->myCurl();

$result = json_decode($data,true);

// pass connection to objects
$transaction = new Transaction($db);

if(!empty($result)) {
	if($transaction_id) {
		// Check if transaction id is exist
		$transaction->trans_id = $transaction_id;
		$checkTransId = $transaction->getTrans();

		if(!empty($checkTransId)) {
			// Update the data
			$transaction->status 		= $result['status'];
			$transaction->receipt 		= $result['receipt'];
			$transaction->time_served 	= $result['time_served'];

			if(!$transaction->update()) {
				$results['response_code'] 	= 500;
				$results['message'] 		= "Failed to update the data";
				$results['data'] 			= $result;
			} else {
				$results['response_code'] 	= 200;
				$results['message'] 		= "Success to update the data";
				$results['data'] 			= $result;
			}
		} else {
			$results['response_code'] 	= 500;
			$results['message'] 		= "We can't find the data with id {$transaction_id}";
			$results['data'] 			= [];
		}
	} else {
		$transaction->trans_id 			= $result['id'];
		$transaction->amount 			= $result['amount'];
		$transaction->status 			= $result['status'];
		$transaction->timestamp 		= $result['timestamp'];
		$transaction->bank_code 		= $result['bank_code'];
		$transaction->account_number 	= $result['account_number'];
		$transaction->beneficiary_name 	= $result['beneficiary_name'];
		$transaction->remark 			= $result['remark'];
		$transaction->receipt 			= $result['receipt'];
		$transaction->time_served 		= $result['time_served'];
		$transaction->fee 				= $result['fee'];

		if(!$transaction->save()) {
			$results['response_code'] 	= 500;
			$results['message'] 		= "The data is not saved into the database";
			$results['data'] 			= $result;
		} else {
			$results['response_code'] 	= 200;
			$results['message'] 		= "The data has been saved into the database";
			$results['data'] 			= $result;
		}
	}
} else {
	$results['response_code'] 	= 500;
	$results['message'] 		= "The data is empty";
	$results['data'] 			= [];
}

$res = json_encode($results,JSON_PRETTY_PRINT);
echo $res;
?>
