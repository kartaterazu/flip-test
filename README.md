## How tu run
1. Open **database.php** on the root of this project.
2. Change database configuration with yours.
3. Run your mysql.
4. Create database on your local machine by run **php migrate.php** using **Command Prompt (windows) / Console (linux) / Terminal (Mac OS)**.
5. Run application by using **Command Prompt (windows) / Console (linux) / Terminal (Mac OS)**.
6. Or you can use **Postman**, you can download **Postman** [here](https://www.getpostman.com/downloads/)

---

## Run using *Command Prompt (Windows) / Console (Linux) / Terminal (Mac OS)*
1. Open **Command Prompt (windows) / Console (linux) / Terminal (Mac OS)**.
2. Path to your project folder.
3. Type **php disburse.php** to get transaction data.
4. Type **php disburse.php -p[transaction_id]** to get transaction status by id *replace [transaction_id] with id shown on previous step {3}*.

---

## Run using *POSTMAN*
1. Run **XAMPP (Windows)**, run apache and mysql.
2. Open **POSTMAN**.
3. Type **http://localhost/flip-test/disburse.php** to get transaction data
4. Type **http://localhost/flip-test/disburse.php?id=[transaction_id]** to get transaction status by id *replace [transaction_id] with id shown on previous step {3}*

---
