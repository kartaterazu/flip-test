<?php
class Transaction{
 
    // database connection and table name
    private $conn;
    private $table_name = "transaction";
 
    // object properties
    public $id;
    public $trans_id;
    public $amount;
    public $status;
    public $timestamp;
    public $bank_code;
    public $account_number;
    public $beneficiary_nam;
    public $remark;
    public $receipt;
    public $time_served;
    public $fee;
 
    public function __construct($db){
        $this->conn = $db;
    }
 
    // used by select drop-down list
    function getTrans() {
        //select transaction data by trans_id
        $query = "SELECT * FROM " . $this->table_name . " WHERE trans_id = ? limit 1";
         
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->trans_id);
        $stmt->execute();
     
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
 
        return $row;
    }

    function save() {
        $query = "
                    INSERT INTO
                        {$this->table_name}
                    (
                        trans_id, 
                        amount, 
                        status,
                        timestamp,
                        bank_code,
                        account_number,
                        beneficiary_name,
                        remark,
                        receipt,
                        time_served,
                        fee
                    )

                    VALUES
                    (
                        :trans_id, 
                        :amount, 
                        :status,
                        :timestamp,
                        :bank_code,
                        :account_number,
                        :beneficiary_name,
                        :remark,
                        :receipt,
                        :time_served,
                        :fee
                    )
        ";
 
        $stmt = $this->conn->prepare($query);
        
        try {
            $this->conn->beginTransaction();

            // posted values
            $this->trans_id          = htmlspecialchars(strip_tags($this->trans_id));
            $this->amount            = htmlspecialchars(strip_tags($this->amount));
            $this->status            = htmlspecialchars(strip_tags($this->status));
            $this->timestamp         = htmlspecialchars(strip_tags($this->timestamp));
            $this->bank_code         = htmlspecialchars(strip_tags($this->bank_code));
            $this->account_number    = htmlspecialchars(strip_tags($this->account_number));
            $this->beneficiary_name  = htmlspecialchars(strip_tags($this->beneficiary_name));
            $this->remark            = htmlspecialchars(strip_tags($this->remark));
            $this->receipt           = htmlspecialchars(strip_tags($this->receipt));
            $this->time_served       = htmlspecialchars(strip_tags($this->time_served));
            $this->fee               = htmlspecialchars(strip_tags($this->fee));
     
            // bind values 
            $stmt->bindParam(":trans_id", $this->trans_id);
            $stmt->bindParam(":amount", $this->amount);
            $stmt->bindParam(":status", $this->status);
            $stmt->bindParam(":timestamp", $this->timestamp);
            $stmt->bindParam(":bank_code", $this->bank_code);
            $stmt->bindParam(":account_number", $this->account_number);
            $stmt->bindParam(":beneficiary_name", $this->beneficiary_name);
            $stmt->bindParam(":remark", $this->remark);
            $stmt->bindParam(":receipt", $this->receipt);
            $stmt->bindParam(":time_served", $this->time_served);
            $stmt->bindParam(":fee", $this->fee);

            if($stmt->execute()) {
                $this->conn->commit();

                return true;
            } else {
                $this->conn->rollback();

                return false;
            }
        } catch(PDOException $e) {
            $this->conn->rollback();

            return false;
        }
    }
    
    function update() {
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    status = :status,
                    receipt = :receipt,
                    time_served = :time_served
                WHERE
                    trans_id = :trans_id";
     
        $stmt = $this->conn->prepare($query);
        
        try {
            $this->conn->beginTransaction();

            // posted values
            $this->status       = htmlspecialchars(strip_tags($this->status));
            $this->receipt      = htmlspecialchars(strip_tags($this->receipt));
            $this->time_served  = htmlspecialchars(strip_tags($this->time_served));
            $this->trans_id     = htmlspecialchars(strip_tags($this->trans_id));
         
            // bind parameters
            $stmt->bindParam(':status', $this->status);
            $stmt->bindParam(':receipt', $this->receipt);
            $stmt->bindParam(':time_served', $this->time_served);
            $stmt->bindParam(':trans_id', $this->trans_id);
         
            // execute the query
            if($stmt->execute()){
                $this->conn->commit();

                return true;
            } else {
                $this->conn->rollback();

                return false;                
            }         
        } catch(PDOException $e) {
            $this->conn->rollback();

            return false;
        }
    }
}
?>